﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SampleWebBotClient.Models.TankBlaster;
using System.Drawing;
using System.Diagnostics;

namespace SampleWebBotClient.Helpers
{
    public class Stig
    {


        private static List<BotMove> Hist = new List<BotMove>();
        private static Random Random = new Random(System.Guid.NewGuid().GetHashCode());

        public Stig()
        {
        }

        internal BotMove CalculateNextMove(BotArenaInfo arenaInfo)
        {

            Debug.WriteLine("======================================================");
            var response = new BotMove();

            /*
             *  Najkrótsza scieżka do wroga.
             */
            var pathToEnemy = GetPath(arenaInfo.BotLocation, arenaInfo.OpponentLocations.First(), arenaInfo.Board);


            if (pathToEnemy == null) return response;

            /*
             *  Strefa zagrożopna wybuchem rakiety. Lub bomby.
             */
            var explosionZone = AllExplosionZone(arenaInfo.Missiles, arenaInfo.Bombs, arenaInfo.Board, arenaInfo.BotLocation);

            /*
             *  Najbliższa strefa bezpieczna. Jest to obszar do okoła strefy zagrożonej. 
             */
            var saveZone = SaveZone(explosionZone, arenaInfo.Board);

            Debug.WriteLine(string.Join(", ", explosionZone));
            Debug.WriteLine(string.Join(", ", saveZone));

            /*
             *  Jeżeli wróg w prostej linji to kierunek w jego strone. W przeciwnym wypadku null. 
             */
            var fireDirection = ClearShotDirection(arenaInfo.BotLocation, pathToEnemy);

            /*
             *  Czy jestem na terenie zagfrożonym wybuchem 
             */
            var isExplosionZone = explosionZone.Contains(arenaInfo.BotLocation);

            /*
             *  Bezpieczna sicierzka do wroga.
             */
            var savePathToEnemy = GetPath(arenaInfo.BotLocation, arenaInfo.OpponentLocations.First(), arenaInfo.Board, (p) => !explosionZone.Contains(p));

            /*
             *   Potencjalnie dobry kieunek strzału. 
             *   Czyli jest to strzał w którego zasięgu jest wróg.
             */
            var potentialExplosionZone = AllDir.Select(d => new KeyValuePair<MoveDirection, List<Point>>(d, ExplosionZone(new Missile()
            {
                MoveDirection = d,
                ExplosionRadius = arenaInfo.GameConfig.MissileBlastRadius,
                Location = AddPoint(arenaInfo.BotLocation, GetVect(d))
            }, arenaInfo.Board, arenaInfo.BotLocation, true))).ToList();

            var selectedPotentialExplosionZone = potentialExplosionZone.Where(ez => arenaInfo.OpponentLocations.Any(e => ez.Value.Contains(e)) && !ez.Value.Contains(arenaInfo.BotLocation)).ToList();

            if (!selectedPotentialExplosionZone.Any())
            {
                selectedPotentialExplosionZone = potentialExplosionZone.Where(ez => arenaInfo.OpponentLocations.Any(e => SaveZone(ez.Value, arenaInfo.Board).Contains(e)) && !ez.Value.Contains(arenaInfo.BotLocation)).ToList();
            }


            if (isExplosionZone)
            {   // Zagrożony teren
                Debug.WriteLine("Jestem w strefie zagrożenia. Uciekam!");
                var evacuationPath = SelectPath(saveZone, arenaInfo.Board, arenaInfo.BotLocation);
                response.Direction = GetDir(arenaInfo.BotLocation, evacuationPath.First());
            }
            /*else if (fireDirection.HasValue)
            {
                Debug.WriteLine("Widze wroga. Strzelam!");
                response.Action = BotAction.FireMissile;
                response.FireDirection = fireDirection.Value;
            }*/
            else if (pathToEnemy.Count > 5)
            {
                Debug.WriteLine("Nie widze wroga. Jadę do niego!");

                if (savePathToEnemy != null)
                    response.Direction = GetDir(arenaInfo.BotLocation, savePathToEnemy.First());
            }
            else if (selectedPotentialExplosionZone != null && selectedPotentialExplosionZone.Any())
            {

                selectedPotentialExplosionZone = selectedPotentialExplosionZone.OrderBy(a => Guid.NewGuid()).ToList(); // Losowanie kolejności

                Debug.WriteLine("Strzelam na oślep!");
                response.Action = BotAction.FireMissile;
                response.FireDirection = selectedPotentialExplosionZone.First().Key;


            }
            else
            {
                Debug.WriteLine("Losowy ruch!");
                var pathToRandom = PathToFreeRandomSpot(arenaInfo.BotLocation, arenaInfo.Board, (p) => !explosionZone.Contains(p));
                if (pathToRandom != null && pathToRandom.Any())
                    response.Direction = GetDir(arenaInfo.BotLocation, pathToRandom.First());
            }

            Hist.Add(response);

            return response;

        }

        /// <summary>
        /// Wybieramy scieżke do losowego wolnego pola.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="map"></param>
        /// <param name="isAvaible"></param>
        /// <returns></returns>
        public List<Point> PathToFreeRandomSpot(Point self, BoardTile[,] map, Func<Point, bool> isAvaible = null)
        {
            //Point? selected = null;
            List<Point> path = null;
            while (path == null)
            {
                var target = new Point(Random.Next(0, map.GetLength(0)), Random.Next(0, map.GetLength(1)));
                if (map[target.X, target.Y] == BoardTile.Empty)
                {
                    path = GetPath(self, target, map, isAvaible);
                }

            }

            return path;
        }


        /// <summary>
        /// Wybieramy scieżkę do najbliższego z punktów
        /// </summary>
        /// <param name="targets"></param>
        /// <param name="map"></param>
        /// <param name="self"></param>
        /// <returns></returns>
        private List<Point> SelectPath(List<Point> targets, BoardTile[,] map, Point self)
        {
            List<Point> best = null;
            foreach (var t in targets)
            {
                var pb = GetPath(self, t, map);
                if (best == null || (pb != null && pb.Count < best.Count)) best = pb;
            }

            return best;
        }


        /// <summary>
        /// Zamienia wektor na kierunek
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        private MoveDirection GetDir(Point p1, Point p2)
        {
            if (p2 == new Point(p1.X - 1, p1.Y)) return MoveDirection.Left;
            else if (p2 == new Point(p1.X + 1, p1.Y)) return MoveDirection.Right;
            else if (p2 == new Point(p1.X, p1.Y + 1)) return MoveDirection.Down;
            else if (p2 == new Point(p1.X, p1.Y - 1)) return MoveDirection.Up;
            else throw new Exception("Bardzo źle!");
        }


        /// <summary>
        /// Zamina kierunek na wektor.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        private Point GetVect(MoveDirection d)
        {
            switch (d)
            {
                case MoveDirection.Down: return new Point(0, 1);
                case MoveDirection.Up: return new Point(0, -1);
                case MoveDirection.Left: return new Point(-1, 0);
                case MoveDirection.Right: return new Point(1, 0);
                default: throw new Exception("Źle!");
            }
        }

        /// <summary>
        /// Lista wszystkich dostepnych kierunków.
        /// </summary>
        public List<MoveDirection> AllDir => Enum.GetValues(typeof(MoveDirection)).Cast<MoveDirection>().ToList();


        /// <summary>
        /// Dodaawniae punktów (wektorów).
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public Point AddPoint(Point p1, Point p2) => new Point(p1.X + p2.X, p1.Y + p2.Y);

        /// <summary>
        /// Mnożenie wektora.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public Point Mull(Point p1, int m) => new Point(p1.X * m, p1.Y * m);



        /// <summary>
        /// Strefa wybuchu wszystkich rakiet.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="map"></param>
        /// <param name="self"></param>
        /// <returns></returns>
        private List<Point> AllExplosionZone(List<Missile> m, List<Bomb> b, BoardTile[,] map, Point self)
        {
            var ezz = new List<Point>();
            foreach (var i in m) ezz.AddRange(ExplosionZone(i, map, self, true, true));
            foreach (var i in b) ezz.AddRange(BombExplosionZone(i, map, self));
            return ezz.Distinct().ToList();
        }

        /// <summary>
        /// Przewidywana dtrefa wybuchu rakiety.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="map"></param>
        /// <param name="self"></param>
        /// <returns></returns>
        private List<Point> ExplosionZone(Missile m, BoardTile[,] map, Point self, bool includeTrajectory = false, bool includeBackword = false)
        {
            var explosionZone = new List<Point>();

            var v = GetVect(m.MoveDirection);
            var p = m.Location;

            if (includeBackword)
            {
                var steps = m.ExplosionRadius;
                var pb = p;
                while (IsOnMap(pb, map) && map[pb.X, pb.Y] == BoardTile.Empty && steps-- >= 0)
                {
                    explosionZone.Add(pb);
                    pb = new Point(pb.X - v.X, pb.Y - v.Y);
                }
            }

            while (IsOnMap(p, map) && map[p.X, p.Y] == BoardTile.Empty)
            {
                if (includeTrajectory) explosionZone.Add(p);
                p = new Point(p.X + v.X, p.Y + v.Y);
            }

            var otherDir = AllDir.Except(new[] { m.MoveDirection }).ToList();

            // Jeden wstecz bo poprzedfni to ściana
            p = new Point(p.X - v.X, p.Y - v.Y);

            foreach (var d in otherDir)
                for (int i = 0; i <= m.ExplosionRadius && IsOnMap(AddPoint(p, Mull(GetVect(d), i)), map); i++)
                    explosionZone.Add(AddPoint(p, Mull(GetVect(d), i)));

            return explosionZone;
        }

        /// <summary>
        /// Zwraca strefe wybuchu bomby
        /// </summary>
        /// <param name="b"></param>
        /// <param name="map"></param>
        /// <param name="self"></param>
        /// <returns></returns>
        private List<Point> BombExplosionZone(Bomb b, BoardTile[,] map, Point self)
        {
            var explosionZone = new List<Point>();

            var p = b.Location;

            foreach (var d in AllDir)
                for (int i = 0; i <= b.ExplosionRadius && IsOnMap(AddPoint(p, Mull(GetVect(d), i)), map); i++)
                    explosionZone.Add(AddPoint(p, Mull(GetVect(d), i)));

            return explosionZone;
        }

        /// <summary>
        /// Wybieramy wszystkie pola, które są wolne, do okoła strtefy wybuchu.
        /// </summary>
        /// <param name="ez"></param>
        /// <param name="map"></param>
        /// <returns></returns>
        private List<Point> SaveZone(List<Point> ez, BoardTile[,] map)
        {
            var sz = new List<Point>();

            foreach (var p in ez)
                foreach (var d in AllDir)
                {
                    var tp = AddPoint(p, GetVect(d));
                    if (IsOnMap(tp, map) && !ez.Contains(tp) && map[tp.X, tp.Y] == BoardTile.Empty)
                    {
                        sz.Add(tp);
                    }

                }

            return sz;
        }




        /// <summary>
        /// Sprawdzamy czy punkt należy do mapy.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="map"></param>
        /// <returns></returns>
        private bool IsOnMap(Point p, BoardTile[,] map)
        {
            return p.X >= 0 && p.X < map.GetLength(0) && p.Y >= 0 && p.Y < map.GetLength(1);
        }

        /// <summary>
        /// Jeżeli ścieżka którą podaliśmy jest linją prostą, to zwracany jest kierunek w stronę konica ścieżki. W przeciwnym wypadku null.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        private MoveDirection? ClearShotDirection(Point p, List<Point> path)
        {
            if (path.All(i => p.X == i.X) || path.All(i => p.Y == i.Y))
                return GetDir(p, path.First());
            return null;
        }


        /// <summary>
        /// Odległość między punktami. Miara taksówkowa.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        private double Distance(Point p1, Point p2) => Math.Abs(p2.X - p1.X) + Math.Abs(p2.Y - p1.Y);


        /// <summary>
        /// Metoda pobiera sąsziadów punktu. 
        /// Zwracane są tylko takie punkty na którea można wjechać.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="map"></param>
        /// <returns></returns>
        private List<Point> Neighbor(Point p, BoardTile[,] map, Func<Point, bool> isAvaible)
        {
            //if (map[p.X + 1, p.Y] == BoardTile.Empty) { }
            var r = new List<Point>();

            foreach (var dx in new[] { 1, 0, -1 })
                foreach (var dy in new[] { 1, 0, -1 })
                    if ((dx != 0 || dy != 0) && (dx == 0 || dy == 0))
                        /*p.X + dx >= 0 && p.X + dx < map.GetLength(0) && p.Y + dy >= 0 && p.Y + dy < map.GetLength(1) */
                        if (IsOnMap(new Point(p.X + dx, p.Y + dy), map) && map[p.X + dx, p.Y + dy] == BoardTile.Empty)
                            if (isAvaible == null || isAvaible(new Point(p.X + dx, p.Y + dy)))
                                r.Add(new Point(p.X + dx, p.Y + dy));


            return r;
        }

        /// <summary>
        /// Znajdowanie najkrótszej scieżki do celu.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="map"></param>
        /// <returns></returns>
        public List<Point> GetPath(Point start, Point goal, BoardTile[,] map, Func<Point, bool> isAvaible = null)
        {
            var closed = new List<Point>();
            var open = new List<Point>() { start };

            // Jest to lista ocen poszczególnych punktów. 
            // Czyli dystans od startu do trgo punktu.
            var g = new Dictionary<Point, double>() { { start, 0 } };

            // Wskazuje dla danego punktu najlepszego popszednika.
            var bestPrev = new Dictionary<Point, Point>();

            // Przewidywany cąłkowity koszt z tego punktu do celu.
            Func<Point, double> f = (Point p) => g[p] + Distance(p, goal);

            while (open.Any())
            {
                var x = open.OrderBy(f).First();
                if (x == goal)
                {
                    var path = new List<Point>();
                    var cp = x;
                    while (cp != start)
                    {
                        path.Add(cp);
                        cp = bestPrev[cp];
                    }
                    path.Reverse();
                    return path;
                }
                open.Remove(x);
                closed.Add(x);

                foreach (var y in Neighbor(x, map, isAvaible))
                    if (!closed.Contains(y))
                    {
                        var ng = g[x] + 1;

                        if (!open.Contains(y))
                        {
                            // Dodajemy wpis.
                            open.Add(y);
                            bestPrev.Add(y, x);
                            g[y] = ng;
                        }
                        else if (ng < g[y])
                        {
                            // Aktualizujemy gorszy wpis.
                            g[y] = ng;
                            bestPrev[y] = x;
                        }
                    }
            }


            return null;
        }
    }
}