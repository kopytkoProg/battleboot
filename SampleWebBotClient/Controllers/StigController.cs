﻿using SampleWebBotClient.Helpers;
using SampleWebBotClient.Models.TankBlaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SampleWebBotClient.Controllers
{
    public class StigController : TankBlasterSimpleBotControllerBase
    {
        Stig aiService = new Stig();

        protected override string Name
        {
            get { return "Stig"; }
        }

        protected override string AvatarUrl
        {
            get { return Url.Content("~/Content/stig_white.png"); }
        }

        public override BotMove PerformNextMove(BotArenaInfo arenaInfo)
        {
           
            var result = aiService.CalculateNextMove(arenaInfo);

            return result;
        }
    }
}